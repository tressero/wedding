## Manual Steps (No CI)
docker build -t onepage:v0 .
docker run -d -p 80:80 onepage:v0
curl localhost:80

## Setting up Docker Runners
https://docs.gitlab.com/runner/install/docker.html

### Creating a Docker user with Docker permissions
`sudo usermod -aG docker gitlab-runner`

### Running priveleged containers for the Runners
https://docs.gitlab.com/runner/install/kubernetes.html#running-privileged-containers-for-the-runners

### Volumes: Used to copy data into containers
#### Option A: Local Volume
```bash
# We'll name it "...lovolume" to separate it from docker volumes (if we have both)
docker run -d --name gitlab-runner-lovolume --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock  gitlab/gitlab-runner:latest

docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.com/" \
  --registration-token "<token_number>" \
  --description "lin-a-a-dev01-lovolume01" \
  --tag-list "dev,01,lovolume" \
  --run-untagged="true" \
  --locked="false" \
  --docker-privileged \
  --access-level="not_protected" # vs ref_protected
```

### Option B: Docker Volume
```bash
# We'll name it "...dovolume" to separate it from local volumes (if we have both)
docker volume create gitlab-runner-config

docker run -d --name gitlab-runner-dovolume --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest

docker run \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v gitlab-runner-config:/etc/gitlab-runner \
  --executor "docker" \
  --docker-image "docker:19.03.11" \
  --url "https://gitlab.com/" \
  --registration-token "{{ token_number }}" \
  --description "{{ gitlab_runner_name }}" \
  --tag-list "dev,docker" \
  --run-untagged="true" \
  --locked="false" \
  --docker-privileged \
  --docker-volumes ["/certs/client","/cache"]
```

### Other Options: Skipping Docker Entirely (local runner)
#### Option 3: Local Runner (Docker based)
```bash
gitlab-runner register -n \
  --executor "docker" \
  --docker-image "docker:19.03.11" \
  --url "https://gitlab.com/" \
  --registration-token "{{ token_number }}" \
  --description "{{ gitlab_runner_name }}" \
  --tag-list "dev,01,runner" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```

#### Option 4: REAL regular runner (SHELL)
```bash
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token "{{ token_number }}" \
  --executor shell \
  --description "{{ gitlab_runner_name }}"
  --tag-list "dev,shell"
sudo usermod -aG docker gitlab-runner
sudo -u gitlab-runner -H docker info
```
