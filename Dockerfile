FROM nginx:alpine
COPY ./site/onepage /var/www/wedding/
COPY ./site/onepage /usr/share/nginx/html
COPY ./nginx/ochsners.us /etc/nginx/sites-enabled/
#EXPOSE 80
#ADD / entrypoint.sh
#ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
# REMOVED BELOW LINE DUE TO ERROR - NOTE: MAY NEED TO RESTART STILL
# Step 6/6 : RUN nginx -s reload
#  ---> Running in cd8401a0a052
# 2020/06/21 10:09:47 [notice] 1#1: signal process started
# 2020/06/21 10:09:47 [error] 1#1: open() "/var/run/nginx.pid" failed (2: No such file or directory)
# nginx: [error] open() "/var/run/nginx.pid" failed (2: No such file or directory)
# The command '/bin/sh -c nginx -s reload' returned a non-zero code: 1
# ERROR: Job failed: exit code 1
#
#RUN nginx -s reload
